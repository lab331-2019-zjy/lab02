import { TestBed, inject } from '@angular/core/testing';

import { StudentDataImplZJYService } from './student-data-impl-zjy.service';

describe('StudentDataImplZjyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentDataImplZJYService]
    });
  });

  it('should be created', inject([StudentDataImplZJYService], (service: StudentDataImplZJYService) => {
    expect(service).toBeTruthy();
  }));
});
