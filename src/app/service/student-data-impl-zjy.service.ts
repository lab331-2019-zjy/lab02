import { Injectable } from '@angular/core';
import { Student } from '../entity/student';
import { Observable, of } from 'rxjs';
import { StudentService } from './student-service';

@Injectable({
  providedIn: 'root'
})
export class StudentDataImplZJYService extends StudentService{

  constructor() { 
    super();
  }
  getStudents(): Observable<Student []>{
    return of(this.students);
  }
  students: Student[]= [{
    'id': 1,
    'studentId': '5921115508',
    'name': 'Junyu',
    'surname': 'Zhou',
    'gpa': 100.00
  }, {
    'id': 2,
    'studentId': '592115508',
    'name': 'Junyu',
    'surname': 'Zhou',
    'gpa': 200.00
  }];

}
