import { Component, OnInit } from '@angular/core';
import {Student} from '../entity/student';
import { StudentService } from '../service/student-service';
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  constructor(private studentService: StudentService) { }
  students: Student[];
  ngOnInit() {
      this.studentService.getStudents().subscribe(
        students => {
          this.students = students;
        }
      );
  }

  averageGpa(): number {
    let sum = 0;
    for (let student of this.students) {
      sum += student.gpa;
    }
    return sum / this.students.length;

  }

}
